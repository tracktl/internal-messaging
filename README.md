# IM

Convenient wrapper for rabbitMQ. Use for communication between micro services.

## import

The source are build with CI and pushed to the branch release.

Therefor, the lib should be imported with `"internal-messaging": "git@bitbucket.org:tracktl/internal-messaging.git#release"` in `package.json`

## API

__init__

```js
import IM from 'internal-messaging'

const im = new IM

// connect to the rabbitMQ instance
im.connect({ host:'localhost', port:5672 })
    .then( ... )

```

__As event emitter__


subscribe to a type of message
```js

const messageType = 'search:alexander'
const handler = ( {payload, meta, err}, ack ) => {

    // do something

    // declare the message as treated
    ack()
})

im.on( messageType, handler )
```

emit a message
```js

const messageType = 'search:alexander'
const payload = {
    query: 'yolo'
}

im.emit( messageType, payload )
```


__As requetter__

Request a resource with promise. Based on messaging.

request something
```js

const messageType = 'search:alexander'
const payload = {
    query: 'yolo'
}

im.request( messageType, payload )
    .then( ... )
```

response to a request
```js


const messageType = 'search:alexander'

// the promise will resolve with the payload
const payload = {
    result: 'swag'
}

// if err is not null, the promise will reject with err
const err = null


// the meta field is passed as arguments in the handler of .on method
const meta = ...

im.resolve( messageType, payload, meta, err )
```

## passing args

The im.connect can be called without option params.

In this case, params should be set as env var.

`IM_PORT_5672_TCP_PORT` and `IM_PORT_5672_TCP_ADDR` will be looked first, ( it correspond to docker binding when the container is started with --link="rabbitmqcontainer:im" )

`IM_PORT` and `IM_HOST` will be looked at in second.

`IM_USER` and `IM_PASS` should be set.


## debug

when build in dev env, IM can spoof message with http request

send request to `localhost:8080/pushMessage` with params in queryString. Available params are type, meta_key, meta_deliverTo. Everything else will be set as payload.

send request to `localhost:8080/pullMessage` to access the last message emited to the IM.
