import { InternalMessaging as IM } from './request'

module.exports = process.env.NODE_ENV == 'production' ? IM : require('./mock')( IM )
