import {connect}    from 'amqplib'
import assert       from 'assert'
import debug        from 'debug'

const log = debug('im')

const buildUrl = (options = {}) => {
    const user = options.user || process.env.IM_USER
    const pass = options.pass || process.env.IM_PASS
    const port = options.port || process.env.IM_PORT_5672_TCP_PORT || process.env.IM_PORT
    const host = options.host || process.env.IM_PORT_5672_TCP_ADDR || process.env.IM_HOST || 'localhost'

    const auth_prefix = user ? user+':'+pass+'@' : ''
    const port_prefix = port ? ':'+port          : ''

    return 'amqp://'+ auth_prefix + host + port_prefix
}

export class InternalMessaging {

    connect( options = {} ){

        const url = buildUrl( options )

        log( `connecting to "${url}"`)

        return connect( url )
            .then( conn => ( this._conn = conn ).createChannel() )
            .then( channel => this._channel = channel )
            .then( () => log( `connected`) )
            .catch( err => {
                log( `connection failed`, err )
                return Promise.reject( err )
            })
    }

    on( messageType, handler, options={} ){

        assert( this._channel, 'rabbit mq not connected' )
        assert( typeof messageType == 'string', 'messageType missing' )

        this._channel.assertQueue( messageType )

        if( !options.noAck )
            this._channel.prefetch( options.prefetch || 5 )

        this._channel.consume(
            messageType,
            m =>
                handler( JSON.parse( m.content.toString() ), () => this._channel.ack( m ) )
            ,
            {noAck: options.noAck}
        )

        return this
    }

    emit( type, payload={}, meta={}, err ) {

        assert( this._channel, 'rabbit mq not connected' )
        assert( typeof type == 'string', 'type missing' )

        const msg = { payload, meta, err }

        this._channel.assertQueue( type )

        this._channel.sendToQueue( type, new Buffer(JSON.stringify(msg)) )

        return this
    }
}
