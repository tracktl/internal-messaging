import { InternalMessaging as Parent } from './emitter'
import assert       from 'assert'

const genKey = () =>
    'k'+Math.random().toString(36).replace(/[^\w]/g, '').slice(2,17)

const bindCallBack = function(){

    this._channel.assertExchange('callback', 'topic')

    return this._channel.assertQueue('', {exclusive: true})
        .then( q => {

            this._channel.bindQueue(q.queue, 'callback', `*.${ this._id }.*`)

            this._channel.consume(q.queue, onCallBack.bind( this ))
        })
}
const onCallBack = function( m ){

    const {err, meta, payload} = JSON.parse(m.content.toString())

    assert( meta && meta.key , 'meta.key unexpected' )

    const p = this._pendingRequest[ meta.key ]

    this._channel.ack( m )

    if ( !p )
        return

    delete this._pendingRequest[ meta.key ]

    err
        ? p.reject( err )
        : p.resolve( payload )
}

export class InternalMessaging extends Parent {

    constructor(){

        super()

        this._pendingRequest = {}
        this._id = genKey()
    }

    connect(  ){
        return super.connect( ...arguments )
            .then( () => bindCallBack.call( this ) )
    }

    resolve( type, payload, meta, err ) {

        assert( this._channel, 'rabbit mq not connected' )
        assert( typeof type == 'string', 'type missing' )
        assert( meta && meta.deliverTo, 'meta.deliverTo missing' )
        assert( meta && meta.key, 'meta.key missing' )

        const msg = { payload, meta: {...meta, type }, err }

        this._channel.assertExchange('callback', 'topic')

        this._channel.publish('callback', `${ type }.${ meta.deliverTo }.${ meta.key }`, new Buffer(JSON.stringify(msg)) )

        return this
    }

    request( type, payload ){

        return new Promise( ( resolve, reject) => {

            const key = genKey()

            this._pendingRequest[ key ] = { resolve, reject }

            this.emit( type, payload, { deliverTo: this._id, key } )
        })
    }
}
