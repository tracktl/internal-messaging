import {createServer} from 'http'
import {parse as parseURL} from 'url'

const server = ( im, options ) =>

    createServer()
        .on('request' , (req, resp) => {

            const url = parseURL( req.url )

            const query = (url.query || '')
                .split('&')
                .reduce( (o ,x) => {
                    const s = x.split('=').map( x => decodeURIComponent( x ) )

                    return { ...o, [s[0]]: s[1]}
                }, {} )

            const meta = {
                key         : query.meta_key,
                deliverTo   : query.meta_deliverTo,
            }

            switch( url.pathname ) {

                case '/pushMessage' :
                    im._handler
                        .filter( x => x.type == query.type )
                        .forEach( x => x.handler( {payload: query, meta} , () => 0 ) )

                    resp.end('pushed')
                    break

                case '/pullMessage' :
                    resp.end(JSON.stringify( im._emitted ))
                    break

                default:
                    resp.end('unexpected route')
            }
        })
        .listen( options.debug_port || 8080, (err) => console.log( err || 'debug server launch on localhost:'+(options.debug_port || 8080) ) )


module.exports = ( IM ) => {

    class _IM extends IM {

        constructor(){
            super()

            this._emitted = []
            this._handler = []
        }

        emit( type, payload, meta, error ){

            this._emitted.push( {type, payload, meta, error, date: Date.now()} )
            while( this._emitted.length > 250 )
                this._emitted.shift()

            try{
                return super.emit( ...arguments )
            } catch( e ){}
        }

        resolve( type, payload, meta, error ){

            this._emitted.push( {callback: true, type, payload, meta, error, date: Date.now()} )
            while( this._emitted.length > 250 )
                this._emitted.shift()

            try{
                return super.resolve( type, payload, meta, error )
            } catch( e ){}
        }

        on( messageType, handler ){

            this._handler.push({ type: messageType, handler })
            try{
                return super.on( ...arguments )
            } catch( e ){}
        }

        connect( options = {} ){
            return super.connect( ...arguments )
                .catch( err => console.log('rabbit mq err', err))
                .then( () => server(this, options) )
        }

    }

    return _IM
}
